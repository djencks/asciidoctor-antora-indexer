= Asciidoctor-antora-indexer Changelog

== v0.1.3-rc2

* Fix contentAs caching.

== v0.1.2-rc2

* Upgrade to report-support 0.1.2-rc.2.

== v0.1.1-rc2

* Set up caching in contentAs/contentExtractor functionality.

== v0.1.0-rc2

* Block expression processor
* `contentAs` feature for supplying parsed contents to template for indexPage.
* Adapt to Antora 3.0.0-rc.2.
* Allow specifying indexPage queries in files in the target component version.
* Allow specifying javascript helper code in the examples family.
* Allow transforming query results (such as applying a sort) before rendering, using a required javascript function.
* (internal) Simplify passing the logging context.
* Switch to yarn 3.1 pnp package management

== v0.1.0-beta.1

* Implement indexPage Antora extension functionality mapping results from a query to pages added to the content catalog.
* Compliant with Antora 3.0.0-beta.1 extensions.

== v 0.1.0-alpha.1

* Move all output generating functionality into `@djencks/asciidoctor-report-support`, leaving only the query functionality in this project.
* Add `requires` functionality so that user-supplied javascript can be applied to most output fields.
* Change to simpler output format specification consistent with other users of `@djencks/asciidoctor-report-support`.
* Add all processors supported by `@djencks/asciidoctor-report-support`.
** `indexExpression` inline processor.
** `index$` include processor.
** `indexcount$` include processor.
** `indexuniquecount$` include processor.
* Change development branch from `master` to `main`.

== v 0.0.8

* Escape relative filenames to avoid quotes substitution (issue 1).
* Support Antora 3 versionless components via 'version=~' (Thanks to Thor K, MR 1).
Generated $xref and $resourceid values use '_@' to indicate a versionless component.
* Add unmodified/uncamelcased AsciiDoc attributes to static-evel context under 'this'.

== v 0.0.7

* Introduce `$resourceid` that can be used in block templates, e.g. to construct an include.
* Introduce `indexTemplate` block that uses the block template to produce a single chunk of text which is then parsed into the document directly.

== v 0.0.6

* Allow lists of relative globs, and allow (fix) excluding relative globs

== v 0.0.5

* Camel-case attribute names when used in format strings.
* Unescape `\*` to `*` in `indexCount` and `indexUniqueCount` inline macro attributes.
* Don't index aliases of current page.
* Add `indexNestedList` and `indexNestedOrderedList` processors.

== v 0.0.4

* Add `indexBlock` block processor for repeating blocks.
* Add format expressions to all processors.
* Add `antora-indexer-log-lists` configuration attribute to print list contents for easier reuse in Antora nav files.
* Refactor list-utils for easy reuse in other plugins.

== v 0.0.3

* dlist and olist support.

== v 0.0.2

* Implement attribute not present filter.

== v 0.0.1

* Initial implementation.
